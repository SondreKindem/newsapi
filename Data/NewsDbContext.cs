﻿using Microsoft.EntityFrameworkCore;
using NewsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsAPI.Data
{
    public class NewsDbContext : DbContext
    {
        public DbSet<Article> Articles { get; set; }
        public DbSet<Source> Sources { get; set; }

        public NewsDbContext(DbContextOptions<NewsDbContext> options): base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Source>().HasData(new Source { Id = 1, Name = "Bergens Tidende", Updated = new DateTime(2020, 1, 6) });
            modelBuilder.Entity<Source>().HasData(new Source { Id = 2, Name = "VG", Updated = new DateTime(2020, 1, 6) });
            modelBuilder.Entity<Source>().HasData(new Source { Id = 3, Name = "E24", Updated = new DateTime(2020, 1, 6) });

            modelBuilder.Entity<Article>().HasData(new Article { Id = 1, Title = "Test article n. 1", Text = "This is a test article", Link = "https://google.com", Image = "https://via.placeholder.com/350x150", Date = new DateTime(), SourceId = 1 });
            modelBuilder.Entity<Article>().HasData(new Article { Id = 2, Title = "Test article n. 2", Text = "This is another test article", Link = "https://google.com", Image = "https://via.placeholder.com/350x150", Date = new DateTime(), SourceId = 2 });
        }
    }
}
