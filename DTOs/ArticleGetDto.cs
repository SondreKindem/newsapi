﻿using NewsAPI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NewsAPI.DTOs
{
    /// <summary>
    /// DTO for renaming text to title and date to created
    /// </summary>
    public class ArticleGetDto
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Summary { get; set; }

        public string Link { get; set; }

        public string Image { get; set; }

        public DateTime Created { get; set; }

        public SourceInArticleDto Source { get; set; }
    }
}
