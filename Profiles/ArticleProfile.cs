﻿using AutoMapper;
using NewsAPI.DTOs;
using NewsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsAPI.Profiles
{
    public class ArticleProfile : Profile
    {
        public ArticleProfile()
        {
            CreateMap<ArticleGetDto, Article>()
                .ForMember((a => a.Text), opt => opt.MapFrom(dto => dto.Summary))
                .ForMember(a => a.Date, opt => opt.MapFrom(dto => dto.Created));
            CreateMap<Article, ArticleGetDto>()
                .ForMember((dto => dto.Summary), opt => opt.MapFrom(a => a.Text))
                .ForMember(dto => dto.Created, opt => opt.MapFrom(a => a.Date));

            CreateMap<ArticleCreateDto, Article>();
        }
    }
}
