﻿using AutoMapper;
using NewsAPI.DTOs;
using NewsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsAPI.Profiles
{
    public class SourceProfile : Profile
    {
        public SourceProfile()
        {
            CreateMap<SourceCreateDto, Source>();
            CreateMap<Source, SourceInArticleDto>();
        }
    }
}
