﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NewsAPI.Models
{
    public class Article
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string Title { get; set; }

        [MaxLength(500)]
        public string Text { get; set; }

        [Required]
        [MaxLength(255)]
        public string Link { get; set; }

        [MaxLength(255)]
        public string Image { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public int SourceId { get; set; }
        public Source Source { get; set; }
    }
}
