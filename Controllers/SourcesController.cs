﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NewsAPI.Data;
using NewsAPI.DTOs;
using NewsAPI.Models;

namespace NewsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SourcesController : ControllerBase
    {
        private readonly NewsDbContext _context;
        private readonly IMapper _mapper;

        public SourcesController(NewsDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Source>> Get()
        {
            return _context.Sources;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Source>> Get(int id)
        {
            var source = await _context.Sources.FindAsync(id);
            return source;
        }

        [HttpPost]
        public async Task<ActionResult<Source>> Post(SourceCreateDto source)
        {
            var convertedSource = _mapper.Map<Source>(source);
            _context.Sources.Add(convertedSource);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Get", new Source { Id = convertedSource.Id }, convertedSource);
        }
    }
}
