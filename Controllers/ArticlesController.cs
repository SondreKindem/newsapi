﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NewsAPI.Data;
using NewsAPI.DTOs;
using NewsAPI.Models;

namespace NewsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticlesController : ControllerBase
    {
        private readonly NewsDbContext _context;
        private readonly IMapper _mapper;

        public ArticlesController(NewsDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<ArticleGetDto>>> Get()
        {
            return _mapper.Map<List<ArticleGetDto>>(await _context.Articles.Include(a => a.Source).ToListAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ArticleGetDto>> GetOne(int id)
        {
            var article = await _context.Articles.Include(a => a.Source).Where(a => a.Id == id).FirstAsync();
            return _mapper.Map<ArticleGetDto>(article);
        }

        [HttpPost]
        public async Task<ActionResult<Article>> Post(ArticleCreateDto article)
        {
            var convertedArticle = _mapper.Map<Article>(article);
            _context.Articles.Add(convertedArticle);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Get", new Article { Id = convertedArticle.Id }, convertedArticle);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, Article article)
        {
            if(id != article.Id)
            {
                return BadRequest();
            }

            _context.Entry(article).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Article>> Delete(int id)
        {
            var article = await _context.Articles.FindAsync(id);

            if(article == null)
            {
                return NotFound();
            }

            _context.Articles.Remove(article);
            await _context.SaveChangesAsync();

            return article;
        }
    }
}
